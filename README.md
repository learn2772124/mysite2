# Django project for CI/CD

This is a simple django rest framework project. The aim is to learn how CI/CD works.

## Installation

It is recommended to use a virtual environment in python to install the
dependencies.

You can use any tool to create a virtual environment.

Here we use `virtualenv`. If you don't have virtualenv installed locally, run
this command:

```bash
pip3 install virtualenv
```

Create a virtual environment named `env`:

```bash
virtualenv env
```

Activate the environment in your shell(UNIX):

```bash
source env/bin/activate
```

Install the project dependencies:

```bash
pip install -r requirements.txt
```

## Usage

### Setup database

For the first setup run the migration command to generate the schema in
the SQLite3 database:

```bash
python3 manage.py makemigrations
```

And then apply the migrations:

```bash
python3 manage.py migrate
```

### Run project

Simply run the project using:

```bash
python3 manage.py runserver
```

## API Documentation

You can check the swagger UI in this url:

`http://localhost:8000/swagger`
